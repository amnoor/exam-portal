from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
def create_app():
    # template_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'templates'))
    # app = Flask(__name__, template_folder=template_dir)
    app = Flask(__name__)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///exams.db'
    
    db.init_app(app)

    # No need for decorator, directly create tables here
    with app.app_context():
        db.create_all()

    # Registering the blueprints
    from .routes import main_bp
    app.register_blueprint(main_bp)

    return app

# Create the database instance without an app instance
db = SQLAlchemy()