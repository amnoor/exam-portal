from . import create_app, db
from .utils import display_question


app = create_app()

app.jinja_env.globals.update(display_question=display_question)

if __name__ == '__main__':
    app.run(debug=True)
