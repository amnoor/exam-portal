from flask_sqlalchemy import SQLAlchemy

from exam_app import db

class Question(db.Model):
    # your model code
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(50))
    content = db.Column(db.String(500))
    answer = db.Column(db.String(200))
    options = db.Column(db.String(500))
