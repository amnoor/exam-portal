from flask import Blueprint, render_template, request, redirect, url_for
from .models import Question
from .utils import evaluate_answer, display_question
from exam_app import db

main_bp = Blueprint('main', __name__)

"""
Flask uses a concept of blueprints for making application components and supporting common patterns within an application 
or across applications. Blueprints can greatly simplify how large applications work and provide a central means for Flask extensions 
to register operations on applications. A Blueprint object works similarly to a Flask application object,but it is not actually 
an application. Rather it is a blueprint of how to construct or extend an application.
"""

@main_bp.route('/')
def index():
    questions = Question.query.all()
    return render_template('index.html', questions=questions)

@main_bp.route('/take_exam')
def take_exam():
    questions = Question.query.all()

    # Sort/group questions by type
    tf_questions = [q for q in questions if q.type == 'tf']
    mcq_questions = [q for q in questions if q.type == 'mcq']
    matching_questions = [q for q in questions if q.type == 'matching']
    fill_in_the_blanks_questions = [q for q in questions if q.type == 'fill_in_the_blanks']

    return render_template('exam.html', 
                           tf_questions=tf_questions,
                           mcq_questions=mcq_questions,
                           matching_questions=matching_questions,
                           fill_in_the_blanks_questions=fill_in_the_blanks_questions)

# @main_bp.route('/submit_exam', methods=['POST'])
# def submit_exam():
#     questions = Question.query.all()
#     score = 0
#     for question in questions:
#         user_answer = request.form.get(str(question.id))
#         if evaluate_answer(user_answer, question.answer, question.type):
#             score += 1
#     return f"You scored {score} out of {len(questions)}"

@main_bp.route('/add_sample_data')
def add_sample_data():
    # Existing questions
    q1 = Question(type='mcq', content='What is the capital of France?', answer='Paris', options='London,Paris,Berlin,Madrid')
    q2 = Question(type='tf', content='The sun is a planet.', answer='False', options='')

    # Matching question
    q3_content = 'Match the following items with their categories:'
    q3_options = 'D. Chair, D. Ball; 4. Room, 5. Ground'
    q3_answer = 'D:4,E:5'
    q3 = Question(type='matching', content=q3_content, answer=q3_answer, options=q3_options)
    
    # Fill in the blanks question
    q4_content = 'The _____ is the largest planet in our solar system.'
    q4_answer = 'Jupiter'
    q4 = Question(type='fill_in_the_blanks', content=q4_content, answer=q4_answer, options='Earth,Venus,Mars,Jupiter')

    # Add to session and commit
    db.session.add_all([q1, q2, q3, q4])
    db.session.commit()
    return "Sample data added"

@main_bp.route('/submit_exam', methods=['POST'])
def submit_exam():
    questions = Question.query.all()
    score = 0
    correct_answers = []
    wrong_answers = []

    for question in questions:
        user_answer = request.form.get(str(question.id))
        if evaluate_answer(user_answer, question.answer, question.type):
            score += 1
            correct_answers.append(question)
        else:
            wrong_answers.append({
                'question': question,
                'user_answer': user_answer
            })

    return render_template('results.html', 
                           score=score, 
                           total=len(questions), 
                           correct_answers=correct_answers, 
                           wrong_answers=wrong_answers)
