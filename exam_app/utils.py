# Utility functions

def display_question(question):
    if question.type == 'mcq':
        return display_mcq(question)
    elif question.type == 'tf':
        return display_tf(question)
    elif question.type == 'matching':
        return display_matching(question)
    elif question.type == 'fill_in_the_blanks':
        return display_fill_in_the_blanks(question)


def display_mcq(question):
    options = question.options.split(',')
    # Return formatted MCQ HTML
    # This will be handled in the exam.html template
    return options

def display_tf(question):
    # Return formatted True/False HTML
    # This will be handled in the exam.html template
    return ['True', 'False']

def display_matching(question):
    options = question.options.split(',')
    # Return formatted Matching HTML
    # This will be handled in the exam.html template
    return options

def display_fill_in_the_blanks(question):
    options = question.options.split(',')
    # Return formatted Fill in the blanks HTML
    # This will be handled in the exam.html template
    return options

def evaluate_answer(user_answer, correct_answer, question_type):
    if question_type == 'mcq':
        return evaluate_mcq(user_answer, correct_answer)
    elif question_type == 'tf':
        return evaluate_tf(user_answer, correct_answer)
    elif question_type == 'matching':
        return evaluate_matching(user_answer, correct_answer)
    elif question_type == 'fill_in_the_blanks':
        return evaluate_fill_in_the_blanks(user_answer, correct_answer)
    
def evaluate_mcq(user_answer, correct_answer):
    return user_answer == correct_answer

def evaluate_tf(user_answer, correct_answer):
    return user_answer == correct_answer

def evaluate_matching(user_answer, correct_answer):
    return user_answer == correct_answer

def evaluate_fill_in_the_blanks(user_answer, correct_answer):
    return user_answer == correct_answer    