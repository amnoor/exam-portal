from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///exams.db'
db = SQLAlchemy(app)

# Step 2: Design the database models

class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(50))
    content = db.Column(db.String(500))
    answer = db.Column(db.String(200))
    options = db.Column(db.String(500))  # For MCQs and matching, saved as comma-separated values

# Step 3: Implement routes to handle the flow of the application

@app.route('/')
def index():
    questions = Question.query.all()
    return render_template('index.html', questions=questions)

# @app.route('/add_sample_data')
# def add_sample_data():
#     q1 = Question(type='mcq', content='What is the capital of France?', answer='Paris', options='London,Paris,Berlin,Madrid')
#     q2 = Question(type='tf', content='The sun is a planet.', answer='False', options='')
#     db.session.add(q1)
#     db.session.add(q2)
#     db.session.commit()
#     return "Sample data added"


@app.route('/take_exam')
def take_exam():
    questions = Question.query.all()
    return render_template('exam.html', questions=questions)

@app.route('/submit_exam', methods=['POST'])
def submit_exam():
    questions = Question.query.all()
    score = 0
    for question in questions:
        user_answer = request.form.get(str(question.id))
        if evaluate_answer(user_answer, question.answer, question.type):
            score += 1
    return f"You scored {score} out of {len(questions)}"

# Step 4: Implement functions to handle different question types

def display_question(question):
    if question.type == 'mcq':
        return display_mcq(question)
    elif question.type == 'tf':
        return display_tf(question)
    elif question.type == 'matching':
        return display_matching(question)
    elif question.type == 'fill_in_the_blanks':
        return display_fill_in_the_blanks(question)

def display_mcq(question):
    options = question.options.split(',')
    # Return formatted MCQ HTML
    # This will be handled in the exam.html template
    return options

def display_tf(question):
    # Return formatted True/False HTML
    # This will be handled in the exam.html template
    return ['True', 'False']

def display_matching(question):
    options = question.options.split(',')
    # Return formatted Matching HTML
    # This will be handled in the exam.html template
    return options

def display_fill_in_the_blanks(question):
    options = question.options.split(',')
    # Return formatted Fill in the blanks HTML
    # This will be handled in the exam.html template
    return options

def evaluate_answer(user_answer, correct_answer, question_type):
    if question_type == 'mcq':
        return evaluate_mcq(user_answer, correct_answer)
    elif question_type == 'tf':
        return evaluate_tf(user_answer, correct_answer)
    elif question_type == 'matching':
        return evaluate_matching(user_answer, correct_answer)
    elif question_type == 'fill_in_the_blanks':
        return evaluate_fill_in_the_blanks(user_answer, correct_answer)

def evaluate_mcq(user_answer, correct_answer):
    return user_answer == correct_answer

def evaluate_tf(user_answer, correct_answer):
    return user_answer == correct_answer

def evaluate_matching(user_answer, correct_answer):
    return user_answer == correct_answer

def evaluate_fill_in_the_blanks(user_answer, correct_answer):
    return user_answer == correct_answer

def create_tables():
    with app.app_context():
        db.create_all()

if __name__ == '__main__':
   # create_tables()  # Creating database tables
    app.jinja_env.globals.update(display_question=display_question)
    app.run(debug=True)

